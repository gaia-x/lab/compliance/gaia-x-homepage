/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        "blueGaiaX":"#000094",
        "purpleGaiaX":"#B900FF",
        "turquoiseGaiaX":"#46DAFF",
        "mediumBlueGaiaX":"#465AFF"
      }
    },
  },
  plugins: [],
}

