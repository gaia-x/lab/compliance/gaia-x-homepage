FROM node:18 AS npm-install

WORKDIR /usr/src/app

COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm install

FROM node:18 AS builder

WORKDIR /usr/src/app

COPY --from=npm-install /usr/src/app/node_modules node_modules
COPY src src
COPY tailwind.config.js tailwind.config.js

RUN npx tailwindcss -i ./src/index.css -o ./dist/output.css --minify

FROM nginx

WORKDIR /usr/share/nginx/html

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /usr/src/app/src/index.html index.html
COPY --from=builder /usr/src/app/dist/output.css dist/output.css
RUN echo "<style>" >> index.html && cat dist/output.css >> index.html && echo "</style>"
